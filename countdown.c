///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Christopher Aguilar
// @date   Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

struct tm referenceTime;



int main(int argc, char* argv[]) {

   time_t t;
   time_t rawtime;
   struct tm * timeinfo;
   time_t dtime;
   

   referenceTime.tm_sec = 0;
   referenceTime.tm_min= 0;
   referenceTime.tm_hour = 0;
   referenceTime.tm_mday = 1;
   referenceTime.tm_mon = 0;
   referenceTime.tm_year = 100;
   referenceTime.tm_wday = 6;
   referenceTime.tm_yday = 0;
   referenceTime.tm_isdst = -1;

   t =  mktime( &referenceTime );
   time ( &rawtime );
   timeinfo = localtime ( &rawtime );
   //printf("The year is %d", referenceTime.tm_year);
   printf("Reference Time:  %s", ctime(&t) );
   
   dtime = difftime( rawtime, t);

   //printf("The difference is %d", dtime);
   
   // we need to pull rawtime every second, find diff time, and deconstruct to display
   int secyear = 31536000;
   int secday = 86400;
   int sechour = 3600;
   int secmin = 60;
   int outyear;
   int outday;
   int outhour;
   int outmin;
   int outsec;

   int mody;
   int modd;
   int modh;
   int mods;



   while(1){
      //printf("This is one second");
      time( &rawtime);
      dtime = difftime( rawtime, t);
      
      outyear = dtime / secyear;
      mody = dtime % secyear;

      outday = mody / secday;
      modd = mody % secday;

      outhour = modd / sechour;
      modh = modd % sechour;

      outmin = modh / secmin;
      mods = modh % secmin;

      outsec = mods;


      printf("\n Years: %d ", outyear);
      printf("Days: %d ", outday);
      printf("Hours: %d ", outhour);
      printf("Minutes: %d ", outmin);
      printf("Seconds: %d ", outsec);

      fflush(stdout);
      sleep(1);
   };

   return 0;
}
